# July 2024, Catarina Aydar - caydar@mpe.mpg.de

#!/bin/bash

# We use Python to create the list of targets that we want to have information from
python3 << 'EOF'

from astropy.table import Table, unique, vstack
from sdss_semaphore.targeting import TargetingFlags

# Reading the spAll file from SDSS
spall = Table.read('spAll-v6_1_3.fits')

# Targeting the Flags in the column that contains information of all the cartons
flags = TargetingFlags(spall['SDSS5_TARGET_FLAGS'])

# Creating a mask with the objects that contain each of the SPIDERS cartons
cartons = ['bhm_spiders_agn_efeds_stragglers', 'bhm_spiders_agn_gaiadr2', 'bhm_spiders_agn_lsdr8', 'bhm_spiders_agn_ps1dr2', 'bhm_spiders_agn_skymapperdr2', \
          'bhm_spiders_agn_supercosmos', 'mwm_erosita_compact_gen', 'mwm_erosita_compact_var', 'mwm_erosita_stars']

mask_bhm_spiders_agn_efeds_stragglers = flags.in_carton_name('bhm_spiders_agn_efeds_stragglers')
bhm_spiders_agn_efeds_stragglers = spall[mask_bhm_spiders_agn_efeds_stragglers]

mask_bhm_spiders_agn_gaiadr2 = flags.in_carton_name('bhm_spiders_agn_gaiadr2')
bhm_spiders_agn_gaiadr2 = spall[mask_bhm_spiders_agn_gaiadr2]

mask_bhm_spiders_agn_lsdr8 = flags.in_carton_name('bhm_spiders_agn_lsdr8')
bhm_spiders_agn_lsdr8 = spall[mask_bhm_spiders_agn_lsdr8]

mask_bhm_spiders_agn_ps1dr2 = flags.in_carton_name('bhm_spiders_agn_ps1dr2')
bhm_spiders_agn_ps1dr2 = spall[mask_bhm_spiders_agn_ps1dr2]

mask_bhm_spiders_agn_skymapperdr2 = flags.in_carton_name('bhm_spiders_agn_skymapperdr2')
bhm_spiders_agn_skymapperdr2 = spall[mask_bhm_spiders_agn_skymapperdr2]

mask_bhm_spiders_agn_supercosmos = flags.in_carton_name('bhm_spiders_agn_supercosmos')
bhm_spiders_agn_supercosmos = spall[mask_bhm_spiders_agn_supercosmos]

mask_mwm_erosita_compact_gen = flags.in_carton_name('mwm_erosita_compact_gen')
mwm_erosita_compact_gen = spall[mask_mwm_erosita_compact_gen]

mask_mwm_erosita_compact_var = flags.in_carton_name('mwm_erosita_compact_var')
mwm_erosita_compact_var = spall[mask_mwm_erosita_compact_var]

mask_mwm_erosita_stars = flags.in_carton_name('mwm_erosita_stars')
mwm_erosita_stars = spall[mask_mwm_erosita_stars]

# Making a table with the selected objects
selected_objects = [bhm_spiders_agn_efeds_stragglers, bhm_spiders_agn_gaiadr2, bhm_spiders_agn_lsdr8, bhm_spiders_agn_ps1dr2, bhm_spiders_agn_skymapperdr2, \
                   bhm_spiders_agn_supercosmos, mwm_erosita_compact_gen, mwm_erosita_compact_var, mwm_erosita_stars]
merged_selected_objects = vstack(selected_objects)

# Excluding repetitions from the selected objects
selected_objects_unique = unique(merged_selected_objects, keys=['CATALOGID', 'MJD'])
print(f'{ len(merged_selected_objects) = }\n', f'{len(selected_objects_unique) = }')

# Making the final table of the selected objects without repetitions
selected_objects_unique.write('selected_objects.fits', format='fits', overwrite=True)

EOF

# We will use STILTS to make the match of the selected objects with the ASTRA and eROSITA data

echo stilts="java -Xmx4096M -jar /home/idies/workspace/erofollowup/sw/TOPCAT_STILTS/stilts.jar"

# Joining the carton target files and excluding repetitions, to later be able to obtain the ero_detuid
stilts tcat \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_efeds_stragglers_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_gaiadr2_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_lsdr8_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_ps1dr2_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_skymapperdr2_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_supercosmos_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/mwm_erosita_compact_gen_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/mwm_erosita_compact_var_0.5.0.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/mwm_erosita_stars_0.5.0.fits \
    icmd='keepcols "catalogid ero_detuid"'\
    ocmd='sort "catalogid ero_detuid"; uniq "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/selected_targetcartons.fits \
    ofmt=fits-basic 
    
# Making the match between the carton targets from eROSITA and the information from SDSS based on CATALOGID to obtain the ero_detuid
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/selected_targetcartons.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/selected_objects.fits \
    icmd1='keepcols "catalogid ero_detuid"' \
    icmd2='keepcols "CATALOGID SDSS_ID FIRSTCARTON FIELD MJD TARGET_INDEX OBJTYPE FIBER_RA FIBER_DEC Z Z_ERR ZWARNING SN_MEDIAN_ALL CLASS SUBCLASS SPEC_FILE RUN2D BP_MAG RP_MAG GAIA_G_MAG RACAT DECCAT COORD_EPOCH PMRA PMDEC PARALLAX WISE_MAG TWOMASS_MAG GUVCAT_MAG"' \
    ofmt=fits-basic \
    matcher=exact \
    values1=catalogid values2=CATALOGID \
    join=all2 find=all \
    fixcols=all suffix1=_ suffix2= \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' \
    ocmd='select "MJD>59382 && MJD!=59760 && MJD!=59755 && MJD!=59746 && MJD!=59736 && MJD!=59727 && MJD!=59716 && MJD!=59713 &! (MJD==59908&&FIELD==30188) &! (MJD==59799&&FIELD==100562) &! (MJD==59796&&FIELD==104656) &! (MJD==59794&&FIELD==104656) &! (MJD==59765&&FIELD==101764) &! (MJD==59790&&FIELD==0) &! (MJD==59784&&FIELD==0) &! (MJD==59615&&FIELD==16165) &! (MJD==59623&&FIELD==20549) &! (MJD==59940&&FIELD==112361) &! (MJD==59950&&FIELD==27530) &! (MJD==59929&&FIELD==104648) &! (MJD==59928&&FIELD==104666) &! (MJD==59905&&FIELD==417994) &! (MJD==59884&&FIELD==100461) &! (MJD==59990&&(FIELD==29606||FIELD==26062)) &! (MJD==59798&&(FIELD==104598||FIELD==100573)) &! (MJD==59733&&(FIELD==100520||FIELD==100542||FIELD==100981||FIELD==0)) &! (MJD==59971&&FIELD==23401) &! (MJD==60003&&FIELD==23402)"' \
    ocmd='delcols "GroupID"; delcols "GroupSize"' \
    ocmd='sort "CATALOGID MJD"; uniq "CATALOGID MJD"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid.fits

# Making the match of the MWM objects to obtain their stellar fit parameters based on CATALOGID
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/astraAllVisitBossNet-0.5.0.fits \
    icmd1='keepcols "ero_detuid-GUVCAT_MAG"' \
    icmd2='keepcols "catalogid teff e_teff logg e_logg fe_h e_fe_h v_rad e_v_rad";' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid_MWM.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=all1 find=best1 \
    fixcols=dups suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; delcols "GroupID"; delcols "GroupSize"'
    
# Making the match of the MWM objects to obtain Haplha EW based on SDSS_ID
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid_MWM.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/astraAllStarLineForest-0.5.0.fits \
    icmd2='keepcols "catalogid sdss_id eqw_h_alpha eqw_percentiles_h_alpha"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid_MWM_Ha.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=SDSS_ID values2=sdss_id \
    join=all1 find=best1 \
    fixcols=dups suffix1= suffix2=_ \
    ocmd='delcols "sdss_id_"; delcols "catalogid_"; delcols "GroupID"; delcols "GroupSize"'
    
# Adding the eROSITA data based on the ero_detuid

# We make the eFEDS matches separately
# For point-like sources
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid_MWM_Ha.fits \
    in2=/home/idies/workspace/erofollowup/SDSSV/catalogdb_v0.5/inputs/V18C_MainSample_BEST_CTPS_03DEC2020_wERO.fits \
    icmd2='keepcols "ID_SRC RA DEC RADEC_ERR EXT ML_FLUX ML_FLUX_ERR DET_LIKE"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_pl.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1="ero_detuid" values2="toString(ID_SRC)" \
    join=1and2 find=all \
    fixcols=all suffix1=_SDSS suffix2=_ERO \
    ocmd='colmeta -name FLUX_ERO ML_FLUX_ERO; colmeta -name FLUX_ERR_ERO ML_FLUX_ERR_ERO; addcol -after ID_SRC_ERO DETUID_ERO "toString(ID_SRC_ERO)"; delcols ID_SRC_ERO' \
    ocmd='addskycoords "fk5" "ecliptic" "RA_ERO" "DEC_ERO" "ECL_LONG" "ECL_LAT"' \
    ocmd='addcol -after RADEC_ERR_ERO MJD_ERO "(ECL_LONG>129.0 && ECL_LONG<=133.6)?58790:(ECL_LONG>133.6 && ECL_LONG<=138.2)?58791:(ECL_LONG>138.2 && ECL_LONG<=142.8)?58792:(ECL_LONG>142.8 && ECL_LONG<147.0)?58793:0"' \
    ocmd='delcols "ECL_LONG"; delcols "ECL_LAT"' \
    ocmd='addcol -after MJD_ERO MJD_FLAG_ERO "0"'

# For extended sources
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid_MWM_Ha.fits \
    in2=/home/idies/workspace/erofollowup/SDSSV/catalogdb_v0.5/inputs/V18C_MainSample_EXT_LIKEgt6lt14_BEST_CTPS_03DEC2020_wERO.fits \
    icmd2='keepcols "ID_SRC RA DEC RADEC_ERR EXT ML_FLUX ML_FLUX_ERR DET_LIKE"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_ext.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1="ero_detuid" values2="toString(ID_SRC)" \
    join=1and2 find=all \
    fixcols=all suffix1=_SDSS suffix2=_ERO \
    ocmd='colmeta -name FLUX_ERO ML_FLUX_ERO; colmeta -name FLUX_ERR_ERO ML_FLUX_ERR_ERO; addcol -after ID_SRC_ERO DETUID_ERO "toString(ID_SRC_ERO)"; delcols ID_SRC_ERO'\
    ocmd='addskycoords "fk5" "ecliptic" "RA_ERO" "DEC_ERO" "ECL_LONG" "ECL_LAT"' \
    ocmd='addcol -after RADEC_ERR_ERO MJD_ERO "(ECL_LONG>129.0 && ECL_LONG<=133.6)?58790:(ECL_LONG>133.6 && ECL_LONG<=138.2)?58791:(ECL_LONG>138.2 && ECL_LONG<=142.8)?58792:(ECL_LONG>142.8 && ECL_LONG<147.0)?58793:0"' \
    ocmd='delcols "ECL_LONG"; delcols "ECL_LAT"' \
    ocmd='addcol -after MJD_ERO MJD_FLAG_ERO "0"'

# For the remaining cartons
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid_MWM_Ha.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/all_e1_201008_poscorr_mpe_clean.fits \
    icmd2='keepcols "DETUID RA DEC RADEC_ERR TSTART TSTOP EXT ML_FLUX_0 ML_FLUX_ERR_0 DET_LIKE_0"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=ero_detuid values2=DETUID \
    join=all1 find=all \
    fixcols=all suffix1=_SDSS suffix2=_ERO \
    ocmd='addcol -before TSTART_ERO MJD_TSTART_ERO "TSTART_ERO/86400"; addcol -before TSTOP_ERO MJD_TSTOP_ERO "TSTOP_ERO/86400"' \
    ocmd='addcol -before TSTART_ERO MJD_ERO "toInteger(51543.875 + (TSTART_ERO+TSTOP_ERO)/172800)"; delcols "TSTART_ERO"; delcols "TSTOP_ERO"' \
    ocmd='addcol -after MJD_ERO MJD_FLAG_ERO "(MJD_TSTOP_ERO-MJD_TSTART_ERO>150)?1:0"' \
    ocmd='delcols "MJD_TSTART_ERO"; delcols "MJD_TSTOP_ERO"' \
    ocmd='colmeta -name FLUX_ERO ML_FLUX_0_ERO; colmeta -name FLUX_ERR_ERO ML_FLUX_ERR_0_ERO; colmeta -name DET_LIKE_ERO DET_LIKE_0_ERO'

# Add them all together, changing some column names
stilts tcat \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_pl.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_ext.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw.fits \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/DL1_SDSSIPL3_eROSITA_complete.fits \
    ocmd='addcol -after EXT_ERO MORPH_ERO "(EXT_ERO>0)?\"EXTENDED\":(EXT_ERO==0)?\"POINTLIKE\":\"--\""; delcols "EXT_ERO"' \
    ocmd='addcol BAND_ERO "\"(0.2-2.3)\""; addcol OBS_SDSS -after SUBCLASS_SDSS "\"APO\""' \
    ocmd='delcols "GroupID"; delcols "GroupSize"' \
    ocmd='replaceval null 99 "ZWARNING_SDSS"; select ZWARNING_SDSS<128; replaceval 99 null "ZWARNING_SDSS"' \
    ocmd='colmeta -name GAIA_BP BP_MAG_SDSS; colmeta -name GAIA_RP RP_MAG_SDSS; colmeta -name GAIA_G GAIA_G_MAG_SDSS' \
    ocmd='colmeta -name RACAT RACAT_SDSS; colmeta -name DECCAT DECCAT_SDSS; colmeta -name COORD_EPOCH COORD_EPOCH_SDSS; colmeta -name PMRA PMRA_SDSS; colmeta -name PMDEC PMDEC_SDSS' \
    ocmd='colmeta -name PARALLAX PARALLAX_SDSS; colmeta -name WISE_MAG WISE_MAG_SDSS; colmeta -name TWOMASS_MAG TWOMASS_MAG_SDSS; colmeta -name GUVCAT_MAG GUVCAT_MAG_SDSS' \
    ocmd='addcol -after RADEC_ERR_ERO POS_ERR_ERO "(RADEC_ERR_ERO==0)?NULL:(RADEC_ERR_ERO>10000)?NULL:(RADEC_ERR_ERO<0)?NULL:sqrt(0.65*RADEC_ERR_ERO*RADEC_ERR_ERO + 0.81)"' \
    ocmd='addcol -after FLUX_ERR_ERO FLUX_ERR_ERO_CORR "(MJD_ERO<58800 && FLUX_ERR_ERO<1e-11)?FLUX_ERR_ERO:(FLUX_ERR_ERO>1e-11)?NULL:(MJD_ERO<58800 && FLUX_ERR_ERO>=1e-11)?NULL:FLUX_ERR_ERO/2"' \
    ocmd='sort "CATALOGID_SDSS MJD_SDSS"; uniq "CATALOGID_SDSS MJD_SDSS"'

# Make the version according to the MoU, adding the metadata of the columns
stilts tpipe\
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/DL1_SDSSIPL3_eROSITA_complete.fits \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/DL1_SDSSIPL3_eROSITA_MoU_bitmask.fits \
    cmd='delcols SPEC_FILE_SDSS; delcols ero_detuid_SDSS; delcols RADEC_ERR_ERO; delcols FLUX_ERR_ERO; delcols FIRSTCARTON_SDSS' \
    cmd='colmeta -name FLUX_ERR_ERO FLUX_ERR_ERO_CORR; colmeta -desc "SDSS CATALOGID (used before the unification with SDSS_ID)" CATALOGID_SDSS; colmeta -desc "SDSS ID (unified for DR19)" SDSS_ID_SDSS; colmeta -desc "SDSS field sequence number" FIELD_SDSS' \
    cmd='colmeta -desc "SDSS modified Julian date of observation" MJD_SDSS; colmeta -desc "SDSS spectrum index" TARGET_INDEX_SDSS; colmeta -desc "SDSS object type" OBJTYPE_SDSS' \
    cmd='colmeta -desc "SDSS fiber position coordinate: right ascension" FIBER_RA_SDSS; colmeta -desc "SDSS fiber position coordinate: declination" FIBER_DEC_SDSS' \
    cmd='colmeta -desc "SDSS best redshift fit" Z_SDSS; colmeta -desc "SDSS redshift error" Z_ERR_SDSS; colmeta -desc "SDSS redshift measurement warning flag" ZWARNING_SDSS' \
    cmd='colmeta -desc "SDSS median Signal to Noise over the entire spectral range" SN_MEDIAN_ALL_SDSS; colmeta -desc "SDSS best fit spectroscopic classification" CLASS_SDSS' \
    cmd='colmeta -desc "SDSS subclass" SUBCLASS_SDSS; colmeta -desc "SDSS observatory (APO or LCO)" OBS_SDSS; colmeta -desc "SDSS Spectro-2D reduction name" RUN2D_SDSS' \
    cmd='addcol -after RUN2D_SDSS VRAD_ASTRA_SDSS "v_rad_SDSS"; delcols "v_rad_SDSS"; addcol -after VRAD_ASTRA_SDSS EVRAD_ASTRA_SDSS "e_v_rad_SDSS"; delcols "e_v_rad_SDSS"' \
    cmd='addcol -after EVRAD_ASTRA_SDSS LOGTEFF_ASTRA_SDSS "teff_SDSS"; delcols "teff_SDSS"; addcol -after LOGTEFF_ASTRA_SDSS ELOGTEFF_ASTRA_SDSS "e_teff_SDSS"' \
    cmd='delcols "e_teff_SDSS"; addcol -after ELOGTEFF_ASTRA_SDSS LOGG_ASTRA_SDSS "logg_SDSS"; delcols "logg_SDSS"; addcol -after LOGG_ASTRA_SDSS ELOGG_ASTRA_SDSS "e_logg_SDSS"' \
    cmd='delcols "e_logg_SDSS"; addcol -after ELOGG_ASTRA_SDSS FEH_ASTRA_SDSS "fe_h_SDSS"; delcols "fe_h_SDSS"; addcol -after FEH_ASTRA_SDSS EFEH_ASTRA_SDSS "e_fe_h_SDSS"; delcols "e_fe_h_SDSS"' \
    cmd='addcol -after EFEH_ASTRA_SDSS EW_HA_ASTRA_SDSS "eqw_h_alpha_SDSS"; delcols "eqw_h_alpha_SDSS"; addcol -after EW_HA_ASTRA_SDSS EEW_HA_ASTRA_SDSS "eqw_percentiles_h_alpha_SDSS"; delcols "eqw_percentiles_h_alpha_SDSS"' \
    cmd='colmeta -desc "ASTRA stellar fit: Radial velocity" VRAD_ASTRA_SDSS; colmeta -desc "ASTRA stellar fit: Radial velocity error" EVRAD_ASTRA_SDSS' \
    cmd='colmeta -desc "ASTRA stellar fit: Effective temperature" LOGTEFF_ASTRA_SDSS; colmeta -desc "ASTRA stellar fit: Effective temperature error" ELOGTEFF_ASTRA_SDSS' \
    cmd='colmeta -desc "ASTRA stellar fit: Surface gravity" LOGG_ASTRA_SDSS; colmeta -desc "ASTRA stellar fit: Surface gravity error" ELOGG_ASTRA_SDSS' \
    cmd='colmeta -desc "ASTRA stellar fit: Metallicity [Fe/H]" FEH_ASTRA_SDSS; colmeta -desc "ASTRA stellar fit: Metallicity [Fe/H] error" EFEH_ASTRA_SDSS' \
    cmd='colmeta -desc "ASTRA stellar fit: H alpha equivalent width" EW_HA_ASTRA_SDSS; colmeta -desc "ASTRA stellar fit: H alpha equivalent width error (percentiles)" EEW_HA_ASTRA_SDSS' \
    cmd='colmeta -desc "Gaia DR2 blue (BP) passband" GAIA_BP; colmeta -desc "Gaia DR2 red (RP) passband" GAIA_RP; colmeta -desc "Gaia DR2 green (G) passband" GAIA_G' \
    cmd='colmeta -desc "Right ascension of the SDSS-V target, as derived from external catalogues" RACAT' \
    cmd='colmeta -desc "Declination of the SDSS-V target, as derived from external catalogues" DECCAT' \
    cmd='colmeta -desc "Coordinate epoch of the SDSS-V target, as derived from external catalogues" COORD_EPOCH' \
    cmd='colmeta -desc "Proper Motion in right ascension of the SDSS-V target, as derived from Gaia DR3" PMRA' \
    cmd='colmeta -desc "Proper Motion in declination of the SDSS-V target, as derived from Gaia DR3" PMDEC; colmeta -desc "Parallax of the SDSS-V target, as derived from Gaia DR3" PARALLAX' \
    cmd='colmeta -desc "WISE photometry (W1, W2, W3, W4)" WISE_MAG; colmeta -desc "2MASS photometry (J, H, Ks)" TWOMASS_MAG; colmeta -desc "GALEX UV photometry (FUV, NUV)" GUVCAT_MAG' \
    cmd='colmeta -desc "eROSITA unique X-ray source identifier" DETUID_ERO' \
    cmd='colmeta -desc "eROSITA position estimate: right ascension" RA_ERO; colmeta -desc "eROSITA position estimate: declination" DEC_ERO' \
    cmd='colmeta -desc "eROSITA positional error" POS_ERR_ERO; colmeta -desc "eROSITA modified Julian date of observation" MJD_ERO' \
    cmd='colmeta -desc "eROSITA morphological classification (point-like or extended)" MORPH_ERO; colmeta -desc "eROSITA flux" FLUX_ERO; colmeta -desc "eROSITA flux error" FLUX_ERR_ERO' \
    cmd='colmeta -desc "eROSITA source detection likelihood" DET_LIKE_ERO; colmeta -desc "eROSITA band for the given flux" BAND_ERO' \
    cmd='colmeta -desc "eROSITA MJD flag (1 for sources close to the boundaries of the survey)" MJD_FLAG_ERO' \
    cmd='colmeta -units "degree" FIBER_RA_SDSS; colmeta -units "degree" FIBER_DEC_SDSS; colmeta -units "km/s" VRAD_ASTRA_SDSS; colmeta -units "km/s" EVRAD_ASTRA_SDSS;' \
    cmd='colmeta -units "degree" RACAT; colmeta -units "degree" DECCAT; colmeta -units "mas/yr" PMRA; colmeta -units "mas/yr" PMDEC; colmeta -units "mas" PARALLAX' \
    cmd='colmeta -units "degree" RA_ERO; colmeta -units "degree" DEC_ERO; colmeta -units "degree" POS_ERR_ERO' \
    cmd='colmeta -units "erg/s/cm^2" FLUX_ERO; colmeta -units "erg/s/cm^2" FLUX_ERR_ERO; colmeta -units "keV" BAND_ERO' \
    cmd='sort "CATALOGID_SDSS DETUID_ERO MJD_SDSS MJD_ERO"; uniq "CATALOGID_SDSS DETUID_ERO MJD_SDSS MJD_ERO"'
