stilts="java -Xmx4096M -jar /home/idies/workspace/erofollowup/sw/TOPCAT_STILTS/stilts.jar"

# Making the match between the targets from eROSITA and the information from SDSS
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/spAll-v6_0_9_IPL2.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/erosita_all_targets_unique_0.5.0.fits \
    icmd1='keepcols "CATALOGID FIRSTCARTON FIELD MJD TARGET_INDEX OBJTYPE FIBER_RA FIBER_DEC Z Z_ERR ZWARNING SN_MEDIAN_ALL CLASS SUBCLASS SPEC_FILE RUN2D GAIA_BP GAIA_RP GAIA_G RACAT DECCAT COORD_EPOCH PMRA PMDEC PARALLAX WISE_MAG TWOMASS_MAG GUVCAT_MAG"' \
    icmd2='keepcols "catalogid carton bitmask"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_bitmask.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; addcol -before FIELD ero_carton "carton_"; delcols "carton_"; colmeta -name ero_bitmask bitmask_'


# Making the match of the MWM objects to obtain their stellar fit parameters
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/sdssv_bossnet.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/erosita_all_targets_unique_0.5.0.fits \
    icmd1='keepcols "catalogid logg logteff feh rv e_logg e_logteff e_feh e_rv";' \
    icmd2='keepcols "catalogid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_MWM_BOSS.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=catalogid values2=catalogid \
    join=1and2 find=all \
    fixcols=dups suffix1=_ suffix2= \
    ocmd='delcols "catalogid_"'

# Adding BOSS data to the catalog with SDSS information of eROSITA targets
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_bitmask.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_MWM_BOSS.fits \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=catalogid values2=catalogid \
    join=all1 find=all \
    fixcols=dups suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; delcols "GroupID"; delcols "GroupSize"; delcols "GroupID_"; delcols "GroupSize_"'

# For each carton, we want to obtain the eRO_detuid from the Target files

## bhm_spiders_agn_efeds_stragglers
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_efeds_stragglers_0.5.0.fits \
    icmd1='select "ero_carton==\"bhm_spiders_agn_efeds_stragglers\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_efeds_stragglers.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## bhm_spiders_agn_gaiadr2
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_gaiadr2_0.5.0.fits \
    icmd1='select "ero_carton==\"bhm_spiders_agn_gaiadr2\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_gaia.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## bhm_spiders_agn_lsdr8
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_lsdr8_0.5.0.fits \
    icmd1='select "ero_carton==\"bhm_spiders_agn_lsdr8\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_ls.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## bhm_spiders_agn_ps1dr2
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_ps1dr2_0.5.0.fits \
    icmd1='select "ero_carton==\"bhm_spiders_agn_ps1dr2\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_ps1.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## bhm_spiders_agn_skymapperdr2
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_skymapperdr2_0.5.0.fits \
    icmd1='select "ero_carton==\"bhm_spiders_agn_skymapperdr2\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_skymapper.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## bhm_spiders_agn_supercosmos
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/bhm_spiders_agn_supercosmos_0.5.0.fits \
    icmd1='select "ero_carton==\"bhm_spiders_agn_supercosmos\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_supercosmos.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## mwm_erosita_compact_gen
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/mwm_erosita_compact_gen_0.5.0.fits \
    icmd1='select "ero_carton==\"mwm_erosita_compact_gen\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_mwm_gen.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## mwm_erosita_compact_var
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/mwm_erosita_compact_var_0.5.0.fits \
    icmd1='select "ero_carton==\"mwm_erosita_compact_var\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_mwm_var.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

## mwm_erosita_stars
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_BOSS.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/cartons/mwm_erosita_stars_0.5.0.fits \
    icmd1='select "ero_carton==\"mwm_erosita_stars\""' \
    icmd2='keepcols "catalogid ero_detuid"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_mwm_stars.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=CATALOGID values2=catalogid \
    join=1and2 find=all \
    fixcols=all suffix1= suffix2=_ \
    ocmd='delcols "catalogid_"; colmeta -name ero_detuid ero_detuid_' 

# Merging the catalogs generated per carton to check if all our objects are with their respective ero_detuid
stilts tcat \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_gaia.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_ls.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_ps1.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_skymapper.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_supercosmos.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_mwm_gen.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_mwm_stars.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_mwm_var.fits \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid.fits

# At last, we make the match to add the eROSITA data

# We make the eFEDS matches separately
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_efeds_stragglers.fits \
    in2=/home/idies/workspace/erofollowup/SDSSV/catalogdb_v0.5/inputs/V18C_MainSample_BEST_CTPS_03DEC2020_wERO.fits \
    icmd2='keepcols "ID_SRC RA DEC RADEC_ERR EXT ML_FLUX ML_FLUX_ERR DET_LIKE"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_pl.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1="ero_detuid" values2="toString(ID_SRC)" \
    join=1and2 find=all \
    fixcols=all suffix1=_SDSS suffix2=_ERO \
    ocmd='colmeta -name FLUX_ERO ML_FLUX_ERO; colmeta -name FLUX_ERR_ERO ML_FLUX_ERR_ERO; addcol -after ID_SRC_ERO DETUID_ERO "toString(ID_SRC_ERO)"; delcols ID_SRC_ERO' \
    ocmd='addskycoords "fk5" "ecliptic" "RA_ERO" "DEC_ERO" "ECL_LONG" "ECL_LAT"' \
    ocmd='addcol -after RADEC_ERR_ERO MJD_ERO "(ECL_LONG>129.0 && ECL_LONG<=133.6)?58790:(ECL_LONG>133.6 && ECL_LONG<=138.2)?58791:(ECL_LONG>138.2 && ECL_LONG<=142.8)?58792:(ECL_LONG>142.8 && ECL_LONG<147.0)?58793:0"' \
    ocmd='delcols "ECL_LONG"; delcols "ECL_LAT"' \
    ocmd='addcol -after MJD_ERO MJD_FLAG_ERO "0"'

stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_agn_efeds_stragglers.fits \
    in2=/home/idies/workspace/erofollowup/SDSSV/catalogdb_v0.5/inputs/V18C_MainSample_EXT_LIKEgt6lt14_BEST_CTPS_03DEC2020_wERO.fits \
    icmd2='keepcols "ID_SRC RA DEC RADEC_ERR EXT ML_FLUX ML_FLUX_ERR DET_LIKE"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_ext.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1="ero_detuid" values2="toString(ID_SRC)" \
    join=1and2 find=all \
    fixcols=all suffix1=_SDSS suffix2=_ERO \
    ocmd='colmeta -name FLUX_ERO ML_FLUX_ERO; colmeta -name FLUX_ERR_ERO ML_FLUX_ERR_ERO; addcol -after ID_SRC_ERO DETUID_ERO "toString(ID_SRC_ERO)"; delcols ID_SRC_ERO'\
    ocmd='addskycoords "fk5" "ecliptic" "RA_ERO" "DEC_ERO" "ECL_LONG" "ECL_LAT"' \
    ocmd='addcol -after RADEC_ERR_ERO MJD_ERO "(ECL_LONG>129.0 && ECL_LONG<=133.6)?58790:(ECL_LONG>133.6 && ECL_LONG<=138.2)?58791:(ECL_LONG>138.2 && ECL_LONG<=142.8)?58792:(ECL_LONG>142.8 && ECL_LONG<147.0)?58793:0"' \
    ocmd='delcols "ECL_LONG"; delcols "ECL_LAT"' \
    ocmd='addcol -after MJD_ERO MJD_FLAG_ERO "0"'

# For the remaining cartons
stilts tmatch2 \
    in1=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_SDSS_erodetuid.fits \
    in2=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/all_e1_201008_poscorr_mpe_clean.fits \
    icmd2='keepcols "DETUID RA DEC RADEC_ERR TSTART TSTOP EXT ML_FLUX_0 ML_FLUX_ERR_0 DET_LIKE_0"' \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw.fits \
    ofmt=fits-basic \
    matcher=exact \
    values1=ero_detuid values2=DETUID \
    join=all1 find=all \
    fixcols=all suffix1=_SDSS suffix2=_ERO \
    ocmd='addcol -before TSTART_ERO MJD_TSTART_ERO "TSTART_ERO/86400"; addcol -before TSTOP_ERO MJD_TSTOP_ERO "TSTOP_ERO/86400"' \
    ocmd='addcol -before TSTART_ERO MJD_ERO "toInteger(51543.875 + (TSTART_ERO+TSTOP_ERO)/172800)"; delcols "TSTART_ERO"; delcols "TSTOP_ERO"' \
    ocmd='addcol -after MJD_ERO MJD_FLAG_ERO "(MJD_TSTOP_ERO-MJD_TSTART_ERO>150)?1:0"' \
    ocmd='delcols "MJD_TSTART_ERO"; delcols "MJD_TSTOP_ERO"' \
    ocmd='colmeta -name FLUX_ERO ML_FLUX_0_ERO; colmeta -name FLUX_ERR_ERO ML_FLUX_ERR_0_ERO; colmeta -name DET_LIKE_ERO DET_LIKE_0_ERO'

# Add them all together, excluding lines with problematic MJD and ZWARNING
stilts tcat \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_pl.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw_agn_efeds_ext.fits \
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/match_sdss_erosita_raw.fits \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/DL1_SDSSIPL2_eROSITA_complete.fits \
    ocmd='addcol -after EXT_ERO MORPH_ERO "(EXT_ERO>0)?\"EXTENDED\":(EXT_ERO==0)?\"POINTLIKE\":\"--\""; delcols "EXT_ERO"' \
    ocmd='addcol BAND_ERO "\"(0.2-2.3)\""; addcol OBS_SDSS -after SUBCLASS_SDSS "\"APO\""' \
    ocmd='delcols "GroupID"; delcols "GroupID_SDSS"; delcols "GroupID_SDSSa"; delcols "GroupSize"; delcols "GroupSize_SDSS"; delcols "GroupSize_SDSSa"' \
    ocmd='colmeta -name GAIA_BP GAIA_BP_SDSS; colmeta -name GAIA_RP GAIA_RP_SDSS; colmeta -name GAIA_G GAIA_G_SDSS' \
    ocmd='colmeta -name RACAT RACAT_SDSS; colmeta -name DECCAT DECCAT_SDSS; colmeta -name COORD_EPOCH COORD_EPOCH_SDSS; colmeta -name PMRA PMRA_SDSS; colmeta -name PMDEC PMDEC_SDSS' \
    ocmd='colmeta -name PARALLAX PARALLAX_SDSS; colmeta -name WISE_MAG WISE_MAG_SDSS; colmeta -name TWOMASS_MAG TWOMASS_MAG_SDSS; colmeta -name GUVCAT_MAG GUVCAT_MAG_SDSS' \
    ocmd='colmeta -name BITMASK_ERO ero_bitmask_SDSS' \
    ocmd='select ZWARNING_SDSS<128' \
    ocmd='select MJD_SDSS>59317; select MJD_SDSS!=59760; select MJD_SDSS!=59755; select MJD_SDSS!=59746; select MJD_SDSS!=59736; select MJD_SDSS!=59727; select MJD_SDSS!=59716' \
    ocmd='select MJD_SDSS!=59713; select "MJD_SDSS!=59908&&FIELD_SDSS!=30188"; select "MJD_SDSS!=59799&&FIELD_SDSS!=100562"; select "MJD_SDSS!=59796&&FIELD_SDSS!=104656"' \
    ocmd='select "MJD_SDSS!=59794&&FIELD_SDSS!=104656"; select "MJD_SDSS!=59765&&FIELD_SDSS!=101764"; select "MJD_SDSS!=59790&&FIELD_SDSS!=0"; select "MJD_SDSS!=59784&&FIELD_SDSS!=0"' \
    ocmd='select "MJD_SDSS!=59615&&FIELD_SDSS!=16165"; select "MJD_SDSS!=59623&&FIELD_SDSS!=20549"; select "MJD_SDSS!=59940&&FIELD_SDSS!=112361"; select "MJD_SDSS!=59950&&FIELD_SDSS!=27530"' \
    ocmd='select "MJD_SDSS!=59929&&FIELD_SDSS!=104648"; select "MJD_SDSS!=59928&&FIELD_SDSS!=104666"; select "MJD_SDSS!=59905&&FIELD_SDSS!=417994"' \
    ocmd='select "MJD_SDSS!=59884&&FIELD_SDSS!=100461"; select "MJD_SDSS!=59990&&(FIELD_SDSS!=29606||FIELD_SDSS!=26062)"' \
    ocmd='select "MJD_SDSS!=59798&&(FIELD_SDSS!=104598||FIELD_SDSS!=100573)"; select "MJD_SDSS!=59733&&(FIELD_SDSS!=100520||FIELD_SDSS!=100542||FIELD_SDSS!=100981||FIELD_SDSS!=0)"' \
    ocmd='addcol -after RADEC_ERR_ERO POS_ERR_ERO "(RADEC_ERR_ERO==0)?NULL:(RADEC_ERR_ERO>10000)?NULL:(RADEC_ERR_ERO<0)?NULL:sqrt(0.65*RADEC_ERR_ERO*RADEC_ERR_ERO + 0.81)"' \
    ocmd='addcol -after FLUX_ERR_ERO FLUX_ERR_ERO_CORR "(MJD_ERO<58800 && FLUX_ERR_ERO<1e-11)?FLUX_ERR_ERO:(FLUX_ERR_ERO>1e-11)?NULL:(MJD_ERO<58800 && FLUX_ERR_ERO>=1e-11)?NULL:FLUX_ERR_ERO/2"' 

# Make the version according to the MoU
stilts tpipe\
    in=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/DL1_SDSSIPL2_eROSITA_complete.fits \
    out=/home/idies/workspace/Storage/CatarinaAydar/persistent/SDSS_eROSITA_DL1/DL1_SDSSIPL2_eROSITA_MoU.fits \
    cmd='delcols FIRSTCARTON_SDSS; delcols ero_carton_SDSS; delcols SPEC_FILE_SDSS; delcols ero_detuid_SDSS; delcols RADEC_ERR_ERO; delcols FLUX_ERR_ERO' \
    cmd='colmeta -name FLUX_ERR_ERO FLUX_ERR_ERO_CORR; colmeta -desc "SDSS ID" CATALOGID_SDSS; colmeta -desc "SDSS field sequence number" FIELD_SDSS' \
    cmd='colmeta -desc "SDSS modified Julian date of observation" MJD_SDSS; colmeta -desc "SDSS unique target identifier" TARGET_INDEX_SDSS; colmeta -desc "SDSS object type" OBJTYPE_SDSS' \
    cmd='colmeta -desc "SDSS fiber position coordinate: right ascension" FIBER_RA_SDSS; colmeta -desc "SDSS fiber position coordinate: declination" FIBER_DEC_SDSS' \
    cmd='colmeta -desc "SDSS best redshift fit" Z_SDSS; colmeta -desc "SDSS redshift error" Z_ERR_SDSS; colmeta -desc "SDSS redshift measurement warning flag" ZWARNING_SDSS' \
    cmd='colmeta -desc "SDSS median Signal to Noise over the entire spectral range" SN_MEDIAN_ALL_SDSS; colmeta -desc "SDSS best fit spectroscopic classification" CLASS_SDSS' \
    cmd='colmeta -desc "SDSS subclass" SUBCLASS_SDSS; colmeta -desc "SDSS observatory (APO or LCO)" OBS_SDSS; colmeta -desc "SDSS Spectro-2D reduction name" RUN2D_SDSS' \
    cmd='addcol -after RUN2D_SDSS RV_BOSS_SDSS "rv_SDSS"; delcols "rv_SDSS"; addcol -after RV_BOSS_SDSS ERV_BOSS_SDSS "e_rv_SDSS"; delcols "e_rv_SDSS"' \
    cmd='addcol -after ERV_BOSS_SDSS LOGTEFF_BOSS_SDSS "logteff_SDSS"; delcols "logteff_SDSS"; addcol -after LOGTEFF_BOSS_SDSS ELOGTEFF_BOSS_SDSS "e_logteff_SDSS"' \
    cmd='delcols "e_logteff_SDSS"; addcol -after ELOGTEFF_BOSS_SDSS LOGG_BOSS_SDSS "logg_SDSS"; delcols "logg_SDSS"; addcol -after LOGG_BOSS_SDSS ELOGG_BOSS_SDSS "e_logg_SDSS"' \
    cmd='delcols "e_logg_SDSS"; addcol -after ELOGG_BOSS_SDSS FEH_BOSS_SDSS "feh_SDSS"; delcols "feh_SDSS"; addcol -after FEH_BOSS_SDSS EFEH_BOSS_SDSS "e_feh_SDSS"; delcols "e_feh_SDSS"' \
    cmd='colmeta -desc "BOSS stellar fit: Radial velocity" RV_BOSS_SDSS; colmeta -desc "BOSS stellar fit: Radial velocity error" ERV_BOSS_SDSS' \
    cmd='colmeta -desc "BOSS stellar fit: Effective temperature" LOGTEFF_BOSS_SDSS; colmeta -desc "BOSS stellar fit: Effective temperature error" ELOGTEFF_BOSS_SDSS' \
    cmd='colmeta -desc "BOSS stellar fit: Surface gravity" LOGG_BOSS_SDSS; colmeta -desc "BOSS stellar fit: Surface gravity error" ELOGG_BOSS_SDSS' \
    cmd='colmeta -desc "BOSS stellar fit: Metallicity [Fe/H]" FEH_BOSS_SDSS; colmeta -desc "BOSS stellar fit: Metallicity [Fe/H] error" EFEH_BOSS_SDSS' \
    cmd='colmeta -desc "Gaia DR2 blue (BP) passband" GAIA_BP; colmeta -desc "Gaia DR2 red (RP) passband" GAIA_RP; colmeta -desc "Gaia DR2 green (G) passband" GAIA_G' \
    cmd='colmeta -desc "Right ascension of the SDSS-V target, as derived from external catalogues" RACAT' \
    cmd='colmeta -desc "Declination of the SDSS-V target, as derived from external catalogues" DECCAT' \
    cmd='colmeta -desc "Coordinate epoch of the SDSS-V target, as derived from external catalogues" COORD_EPOCH' \
    cmd='colmeta -desc "Proper Motion in right ascension of the SDSS-V target, as derived from Gaia DR3" PMRA' \
    cmd='colmeta -desc "Proper Motion in declination of the SDSS-V target, as derived from Gaia DR3" PMDEC; colmeta -desc "Parallax of the SDSS-V target, as derived from Gaia DR3" PARALLAX' \
    cmd='colmeta -desc "WISE photometry (W1, W2, W3, W4)" WISE_MAG; colmeta -desc "2MASS photometry (J, H, Ks)" TWOMASS_MAG; colmeta -desc "GALEX UV photometry (FUV, NUV)" GUVCAT_MAG' \
    cmd='colmeta -desc "Bitmask for the cartons of the targeting" BITMASK_ERO; colmeta -desc "eROSITA unique X-ray source identifier" DETUID_ERO' \
    cmd='colmeta -desc "eROSITA position estimate: right ascension" RA_ERO; colmeta -desc "eROSITA position estimate: declination" DEC_ERO' \
    cmd='colmeta -desc "eROSITA positional error" POS_ERR_ERO; colmeta -desc "eROSITA modified Julian date of observation" MJD_ERO' \
    cmd='colmeta -desc "eROSITA morphological classification (point-like or extended)" MORPH_ERO; colmeta -desc "eROSITA flux" FLUX_ERO; colmeta -desc "eROSITA flux error" FLUX_ERR_ERO' \
    cmd='colmeta -desc "eROSITA source detection likelihood" DET_LIKE_ERO; colmeta -desc "eROSITA band for the given flux" BAND_ERO' \
    cmd='colmeta -desc "eROSITA MJD flag (1 for sources close to the boundaries of the survey)" MJD_FLAG_ERO' \
    cmd='colmeta -units "degree" FIBER_RA_SDSS; colmeta -units "degree" FIBER_DEC_SDSS; colmeta -units "km/s" RV_BOSS_SDSS; colmeta -units "km/s" ERV_BOSS_SDSS;' \
    cmd='colmeta -units "degree" RACAT; colmeta -units "degree" DECCAT; colmeta -units "mas/yr" PMRA; colmeta -units "mas/yr" PMDEC; colmeta -units "mas" PARALLAX' \
    cmd='colmeta -units "degree" RA_ERO; colmeta -units "degree" DEC_ERO; colmeta -units "degree" POS_ERR_ERO' \
    cmd='colmeta -units "erg/s/cm^2" FLUX_ERO; colmeta -units "erg/s/cm^2" FLUX_ERR_ERO; colmeta -units "keV" BAND_ERO' \
    cmd='sort "CATALOGID_SDSS DETUID_ERO MJD_SDSS MJD_ERO"; uniq "CATALOGID_SDSS DETUID_ERO MJD_SDSS MJD_ERO";'
